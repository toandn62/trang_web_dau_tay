var map;
function initAutocomplete() {
  var lat = 10.762622;
  var lng = 106.660172;
  map = new google.maps.Map(document.getElementById("gmap"), {
    center: { lat: lat, lng: lng },
    disableDefaultUI: true,
    zoom: 15,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  });

  var _0x8faf = [
    "\x4C\x6F\x63\x61\x74\x69\x6F\x6E",
    "\x31\x39\x6F\x52\x6F\x42\x61\x76\x36\x75\x68\x43\x5A\x61\x61\x4B\x5F\x7A\x59\x33\x71\x70\x47\x71\x78\x36\x45\x62\x79\x46\x53\x4A\x57\x45\x78\x53\x61\x43\x48\x49\x6D",
    "\x6D\x61\x70\x73"
  ];
  var layer = new google[_0x8faf[2]].FusionTablesLayer({
    query: { select: _0x8faf[0], from: _0x8faf[1] }
  });
  //

  layer.setMap(map);
  getLocation();

  // Create the search box and link it to the UI element.
  var input = document.getElementById("pac-input");
  var searchBox = new google.maps.places.SearchBox(input);
  map.controls[google.maps.ControlPosition.TOP_RIGHT].push(input);

  // Bias the SearchBox results towards current map's viewport.
  map.addListener("bounds_changed", function() {
    searchBox.setBounds(map.getBounds());
  });

  var markers = [];
  // [START region_getplaces]
  // Listen for the event fired when the user selects a prediction and retrieve
  // more details for that place.
  searchBox.addListener("places_changed", function() {
    var places = searchBox.getPlaces();

    if (places.length == 0) {
      return;
    }

    // Clear out the old markers.
    markers.forEach(function(marker) {
      marker.setMap(null);
    });
    markers = [];

    // For each place, get the icon, name and location.
    var bounds = new google.maps.LatLngBounds();
    places.forEach(function(place) {
      var icon = {
        url: place.icon,
        size: new google.maps.Size(71, 71),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(17, 34),
        scaledSize: new google.maps.Size(25, 25)
      };

      // Create a marker for each place.
      markers.push(
        new google.maps.Marker({
          map: map,
          icon: icon,
          title: place.name,
          position: place.geometry.location
        })
      );

      if (place.geometry.viewport) {
        // Only geocodes have viewport.
        bounds.union(place.geometry.viewport);
      } else {
        bounds.extend(place.geometry.location);
      }
    });
    map.fitBounds(bounds);
  });
  // [END region_getplaces]
}

function getLocation() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(showPosition);
  } else {
    // initMap(10.762622,106.660172)
    alert("Geolocation is not supported by this browser.");
  }
}

function showPosition(position) {
  var lat = position.coords.latitude;
  var lng = position.coords.longitude;
  //
  var center = new google.maps.LatLng(lat, lng);
  map.panTo(center);
  map.setZoom(19);
  //
  var marker = new google.maps.Marker({
    position: center,
    map: map,
    title: "My location"
  });
  //
}